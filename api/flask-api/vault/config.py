import os
import json

with open('/etc/password-vault.conf', 'r') as configuration:
    access = json.load(configuration)

class Config:
    DATAKEY = access['keys']['data']
    AUTHKEY = access['keys']['auth']
    DATABASE_URI = access['credentials']

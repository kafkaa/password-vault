import os
import jwt
from flask import Flask
from flask import request
from flask import jsonify
from vault.config import Config
from vault.dbs.mashadb import MashaDB

app = Flask(__name__)
app.config.from_object(Config)

DATAKEY = app.config['DATAKEY']
AUTHKEY = app.config['AUTHKEY']
DATABSE = app.config['DATABASE_URI']


def access():
    try:
        token = request.headers['x-access-token']
        authorized = jwt.decode(token, AUTHKEY, algorithms="HS256")
        data = request.get_json(force=True)
        decoded = jwt.decode(data, DATAKEY, algorithms="HS256")
        return decoded

    except (KeyError, jwt.exceptions.InvalidTokenError):
        return False


@app.route('/store', methods=["POST"])
def store():
    authorized = access()
    if authorized:
        alias = authorized.get('alias')
        with MashaDB(**DATABSE) as db:
            if db.vault.record_exists('alias', alias):
                return jsonify({'status': f"A record already exists for alias {alias}."})
            db.vault.write(**authorized)
        return jsonify({'status': f"record {alias} sucessfully stored on the server."})
    return jsonify({'status': '404 unauthorized'})


@app.route('/fetch', methods=["POST"])
def fetch():
    authorized = access()
    if authorized:
        alias = authorized.get('alias')
        with MashaDB(**DATABSE) as db:
            if not db.vault.record_exists('alias', alias):
                return jsonify({'status': f"no record found for alias {alias}."})
            result = db.vault.select('value').where(**authorized)
        return jsonify({'value': result[0][0]})
    return jsonify({'status': '404 unauthorized'})


@app.route('/list', methods=["POST"])
def list():
    authorized = access()
    if authorized:
        with MashaDB(**DATABSE) as db:
            entries = db.vault.select('name', 'alias').all()
            return jsonify({'entries': entries})
    return jsonify({'status': '404 unauthorized'})


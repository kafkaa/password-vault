#!/usr/local/bin/python3
"""password 1.1.0 [2020-04-21]

a simple cloud-based password manager

USAGE:
    password [command] [--options]

COMMANDS:
    vault       : store and retrieve passwords from the vault
    generate    : generate encryption keys and random passwords

DOCS:
    password command help

"""
import sys
import importlib

this = sys.modules[__name__]

if __name__ == '__main__':

    try:
        command = sys.argv[1]
        module = importlib.import_module(f'src.cmds.{command}')
        command = getattr(module, command)
        command = command(sys.argv[1:])
        command.execute()

    except (IndexError, ModuleNotFoundError):
        print(this.__doc__)

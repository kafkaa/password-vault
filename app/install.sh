#!/bin/sh
set -eu
#set -o pipefail
export DEBIAN_FROTEND=noninteractive

apt-get update
apt-get -y upgrade
apt-get -y install --no-install-recommends xclip zsh
rm -rf /var/lib/apt/lists/*

echo 'setopt PROMPT_SUBST' > /home/password-api/.zshrc
echo 'PS1="[%F{red}debian%f.%F{yellow}linux%f] %F{green}password-api%f %# "' >> /home/password-api/.zshrc

chown -R password-api:password-api * .
chmod u+x main.py 

cd /usr/local/bin
ln -s /home/password-api/app/main.py password
cd $HOME


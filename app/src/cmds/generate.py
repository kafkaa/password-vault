"""password 1.1.0 [2020-04-21]

COMMAND:
    generate

USAGE:
    password generate help
    password generate random [size]
    password generate key [filename]
    
"""
import sys
from secrets import choice
from src.utils import setopts
from src.utils.fileops import CHARS
from src.utils.fileops import keygen

this = sys.modules[__name__]


class generate(setopts.SetOpts):
    """password 1.1.0 [2020-04-21]

GENERATE:

password generate help              : displays this documentation
password generate key [filename]    : generate an encryption key with specified filename
password generate random [size]     : generate a strong random password of length [size]

EXAMPLES:

password generate key bank          : generates an auth key named bank
pasword generate random 32          : generates a password aceptable random string of 32 chars
    
"""
    def execute(self):
        self.validate(this.__doc__)
        self.docs(self.__doc__)

        if self.options.cmd == 'random':
            size = int(self.options.size)
            sys.stdout.write(f"{''.join(choice(CHARS) for _ in range(size))}\n")
            sys.exit()

        if self.options.cmd == 'key':
            operation = keygen(self.options.filename)
            if operation:
                sys.stdout.write(f'generated src/private/{self.options.filename}.rsa\n')
                sys.stdout.write('Don\'t lose this key! Keep if safe!\n')
                sys.exit()
            sys.exit(1)

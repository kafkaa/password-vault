"""password 1.1.0 [2020-04-21]

COMMAND:
    vault

USAGE:
    password vault help
    password vault list
    password vault store [password-name] [password-alias] [password] [-k encryption-key] (optional)
    password vault fetch [password-alias] [-k encryption-key] (optional)
    
"""
import os
import sys
from src.utils import apiops
from src.utils import setopts
from src.utils import fileops

this = sys.modules[__name__]


class vault(setopts.SetOpts):
    """password vault 1.1.0 [2020-04-21]

VAULT:

    password vault help                       : displays this documentation
    password vault list                       : list the contents of the vault
    password vault store                      : store a new password in the vault
    password vault fetch [-c] [-k, keyfile]   : fetch a password stored in the vault

USAGE:

    store password using full name and alias:
    password vault store nationsbank nbk 123456789

    fetch password using the password alias:
    password vault fetch nbk

    use -c flag to fetch password and copy directly to clipboard:
    password vault fetch nbk -c

    use -k keyfile to fetch a password with a unique encrypyion key:
    password vault fetch nbk -k /path/to/encryption/key

    store and fetch each password using a unique encryption key:

    password vault store nationsbank nbk 123456789 -k bnk.rsa
    password vault fetch nbk -k bank.rsa
    

"""
    def execute(self):
        self.validate(this.__doc__)
        self.docs(self.__doc__)

        if self.options.cmd == 'store':
            KEY = self.options.k if self.options.k else fileops.DEFAULT
            password = fileops.encrypt(self.options.password, keyfile=KEY)
            if password:
                data = dict(name=self.options.name, alias=self.options.alias, value=password.decode())
                response = apiops.upstream(apiops.SURL, data)
                sys.stdout.write(f"{response.get('status')}\n")
                sys.exit()
            sys.exit(fileops.EncryptionError)

        if self.options.cmd == 'fetch':
            data = dict(alias=self.options.alias)
            response = apiops.upstream(apiops.FURL, data)
            if 'value' in response:
                password = response.get('value')
                KEY = self.options.k if self.options.k else fileops.DEFAULT
                result = fileops.decrypt(password.encode(), keyfile=KEY)
                if result:
                    if self.options.c:
                        os.system(f"echo '{result}' {fileops.clipboard()}")
                        sys.stdout.write(f"password copied to the clipboard\n")
                        sys.exit()
                    else:
                        sys.stdout.write(f"{result}\n")
                        sys.exit()

                sys.exit(fileops.EncryptionError)
            sys.exit(f"{response.get('status')}")

        if self.options.cmd == 'list':
            response = apiops.upstream(apiops.LURL, dict(request='entries'))
            if 'entries' in response:
                data = response.get('entries')
                if data:
                    for value in data:
                        sys.stdout.write(f'{value[0]}: {value[1]}\n')
                    sys.exit()
                sys.exit('the password vault is empty')

            sys.exit(f"{response.get('status')}")


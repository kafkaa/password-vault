import os
import sys
import platform
import contextlib
from string import ascii_letters
from string import digits, punctuation
from cryptography.fernet import Fernet
from cryptography.fernet import InvalidToken

HOME = sys.path[0]
KEY = f'{HOME}/src/private'
DEFAULT = f'{HOME}/src/private/private.rsa'
CHARS = f"{ascii_letters}{digits}{punctuation}"

EncryptionError = ("An error had occured. "
                   "Please verify the encryption key or the intergity of your data.")

def devnull(func):
    """decorator to redirect stderr to /dev/null"""
    def nullified(*args, **kwargs):
        with open(os.devnull, 'w') as null:
            with contextlib.redirect_stderr(null):
                func(*args, **kwargs)
    return nullified

def clipboard():
    """returns copy to clipboard command based on OS [Darwin|Linux]"""
    system = platform.system()
    if system.lower() == 'darwin':
        return "| pbcopy"
    return "| xclip -sel clip"


def setpath(path, default=KEY):
    base, file = os.path.split(path)
    if not base:
        base = default
    return f"{base}/{file}.rsa"


def keygen(file):
    """generate a new private key;
       stores key in the default location if no path is specified"""
    choice = True
    file = setpath(file)
    if os.path.isfile(file):
        overwrite = input(f'Overwrite {file}? (y, N): ')
        if overwrite in ('n', 'N'):
            choice = False

    if choice:
        with open(file, 'wb') as file:
            file.write(Fernet.generate_key())
    return choice


def encrypt(data, keyfile=None):
    """encrypt data using a private key"""
    with open(keyfile, 'rb') as file:
        key = file.read()
        try:
            return Fernet(key).encrypt(data.encode())
        except InvalidToken:
            return None


def decrypt(token, keyfile=None):
    with open(keyfile, 'rb') as file:
        key = file.read()
        try:
            return Fernet(key).decrypt(token).decode()
        except InvalidToken:
            return None

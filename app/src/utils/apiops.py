import os
import jwt
import json
import secrets
import requests
from random import choice

SURL = 'http://api:5000/store'
FURL = 'http://api:5000/fetch'
LURL = 'http://api:5000/list'
DATAKEY = os.environ.get('DATAKEY')

with open('/home/password-api/app/src/utils/user_agents.json', 'r') as agents:
    USER_AGENTS = json.load(agents)


def random_user_agent(*user_devices):
    """Generate a random http request header

       ARGUMENTS:
           user_devices: str

           Can be any or all of the following:

            'mobile',
            'desktop',
            'smart_box',,
            'game_consoles',
            'crawlers',
            'ereaders'
    """
    browsers = USER_AGENTS[choice(user_devices)]
    return {'User-Agent': browsers[choice(list(browsers))]}


def authorize():
    AUTHKEY = os.environ.get('AUTHKEY')
    token = jwt.encode(dict(token=secrets.token_urlsafe(32)), AUTHKEY, algorithm="HS256")
    return {**random_user_agent('desktop'), 'x-access-token': token}


def upstream(url, payload):
    try:
        payload = jwt.encode(payload, DATAKEY, algorithm="HS256")
        resp = requests.post(url, headers=authorize(), data=json.dumps(payload))
        resp.raise_for_status()
        return resp.json()

    except requests.exceptions.RequestException as e:
        return {'status': e.args[0]}

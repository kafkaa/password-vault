## cloud-based password manager 

fetch and store encrypted passwords using a simple RESTful API

## Overview

password uses a simple RESTful API to store and retrieve passwords to and from a 
remote server. This app uses docker-compose to instantiate an aggregate of services
comprised of docker containers. The three services are the API, implemented using
the Python-Flask framework, the database, using MariaDB and the user command-line 
interface also implemented using Python.

## Requirements

+ [Docker](https://docker.com/)
+ [docker-dompose](https://docs.docker.com/compose/)

## Getting Started
    
    git clone https://gitlab.com/kafkaa/password-vault.git 
    cd password-api
    docker-compose up

#### in a new terminal window run:
    
    docker exec -it password-APP zsh
    
    $-> password help

## Commands

    vault:      store and retrieve passwords to and from the vault
    generate:   generate encryption keys and passwords

## Usage

    password [command] [--options]

#### generate and store a password:
    
    password store amazon amz $(password generate random 32)
    
#### generate and store a password using a specific encryption key:

    password store amazon amz $(password generate random 32) -k /path/to/key.rsa

#### fetch a password from the vault:

    password fetch amz

#### fetch a password for the vault using a specific encryption key:

    password fetch amz -k /path/to/encryption/key.rsa

#### fetch a password from the vault and copy directly to the system clipboard:

    password fetch amz -c
    
## Docs

    password help
    password [command] help

## Issues

+ vault copy to clipboard not working in linux docker container. 
+ needs verbose mode for convenient debugging

## Reference

+ [flask](https://flask.palletsprojetcs.com)
+ [mariadb](https://mariadb.org)
+ [docker](https://docker.com)
+ [docker-compose](https://docs.docker.com/compose/)

## License

+ [Apache](http://opensource.org/licenses/Apache-2.0)
